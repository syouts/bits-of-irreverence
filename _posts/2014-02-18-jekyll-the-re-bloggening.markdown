---
layout: post
title: "Jekyll: The Re-bloggening"
date: 2014-02-18 02:21
categories: blog
---

Redid this blog today using [Jekyll](http://jekyllrb.com/) and some basic CSS. I was getting sick of [Octopress](http://octopress.org/) getting in my way.

Now it's lighter-weight and more easily tweaked. Somewhere in here there's a post about how frameworks are great until they start to suck.
