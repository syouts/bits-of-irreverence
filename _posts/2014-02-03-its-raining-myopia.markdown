---
layout: post
title: "It's Raining Myopia"
date: 2014-02-03 14:51
categories: blog
---

[ArtForum](http://artforum.com/inprint/issue=201402&id=45005)

I was awoken this morning at 8:15 by the rays of sunlight which permeated my open window. With a groan I rolled out of bed and stepped over to my desk to procure a water bottle. The hangover was just kicking in. The grim aftermath of last night's endeavor took the form of an empty six pack on the floor next to my mattress. I rolled two Advil into my left hand and washed them down by chugging my water. I didn't realize how parched I was until I'd finished. Slumping into my chair, I wondered if LA's morning weather was as crisp and cool as it was the previous evening -- but it was impossible to tell given the heat seeping off a rack of mining rigs running full-bore. I settled for the notion that, as I was neither freezing my ass off nor sweaty as balls, it must have been cool enough to keep things just a bit warmer-than-ideal.

A flick of my wrist stirred my mouse from its hibernating position. My trio of monitors flickered to life and I was presented with XScreenSaver's login prompt. A few quick keystrokes and I'd punched in my password. A PDF detailing an assignment for one of my courses was open on the central monitor. I'd get to that later, though. It was the start of my day and I had to update myself on the latest goings-on in the crypto world. The catchphrase for the latest Wall Street film is that "money never sleeps," which probably references the long hours IBankers and the like put in on the job. But in the world of crypto-currency, that phrase is more literal. There are no market hours. The exchanges operate 24/7, and so does the hardware that makes everything tick.

That's where I come in. I'm what they call a miner. My rack of GPUs is a microcosm of the substrate that powers crypto-currency. Graphics Processing Units -- a few years ago, these things were just used for rendering in videogames. Now, a collection of them functions as a sort of consumer-grade supercomputer. And miners are buying them up like hotcakes, causing price inflation. A daily ritual of mine entails checking various online retailers for any deals on additional GPUs. It's often a fruitless endeavor, but occasionally I find something for which the price is right.

I glance over to my rightmost screen, which is busy projecting a wash of neon green text over a transparent terminal. This is my remote interface to the hardware on my shelf. I note that temperatures are acceptable and my hashrates meet expectations. Overheating GPUs result in busted hardware and potential downtime, which in turn results in lower profits. Faster hashrates generate higher profits, since they're indicative of greater computational power, which yields better payouts. Good, I think to myself.

The next order of business is checking my various crypto-currency wallets. A quick click of the mouse and one of these wallets appears on the screen. It's the only relevant wallet at the moment, because it's storing a few hundred thousand of the most profitable currency.

Wow.

Much coin.

How money.

So crypto.

Plz mine.

V rich.

Very currency.

Wow.

A stylized image of a Shiba Inu emblazoned over a golden coin contains these words around its periphery, like some kind of arcane incantation. This wallet stores Dogecoin, which I'm currently mining. It's a crypto-currency that initially gained traction on Reddit as a parody of Bitcoin. Shibes, as Dogecoin afficianados are called, were jokingly screaming "to the moon" as failed Bitcoin speculators considered suicide hotlines in the wake of the Thanksgiving bubble. And now Dogecoin has taken off. Digital money based around a meme involving Comic Sans and a cute dog, exchanging to USD at a rate of 1k Doge per $1.50 or so. That's a better exchange rate than that of some currencies backed by governments. It's a currency that has sent athletes to the Olympics, amongst other charitable works. This is real life. This is strangeness of a high order.

The Skype icon on my dock blinks. I open the window and check my messages. My girlfriend has just inquired, "Hey, are you up?" I respond, "Yeah, what's going?" We spend a few minutes chatting. She asks me if I read the article she emailed me. I didn't. I open a new tab and check GMail. I follow the link.

"ARTFORUM" greets me with a bold-faced, all-caps logo on the top right of this page. The title of the article is "More Problems More Money," and it's about Bitcoin. Specifically, "Finn Brunton on Bitcoin." Well, okay. That name doesn't mean anything to me, but let's keep reading.

> BITCOIN IS THE MONETARY VERSION of a gnostic heresy: an alternate financial cosmology full of secret names and evocations, with a masked prophet in the pseudonymous “Satoshi Nakamoto” (whose real identity, whether singular or a collective plural, remains unknown). Arguments in comment threads and discussion boards are bright with millennial fervor for the end of authority based in governmental scripture and the advent of a new fiscal age founded on cryptographic work and the steady roar of cooling fans. “Mining rigs,” computers built to do nothing but solve special mathematical problems that hunt for bitcoins—essentially, numbers that are very hard, even for a computer, to calculate—warm up basements and server rooms from Sydney to Nashville. In Hong Kong, “Block Erupter” circuit boards immersed in tanks of coolant eat electricity and generate heat and possible solutions; in Iceland, under the aurora, currents of arctic air prevent meltdowns while the machines work. This is strangeness of a high order.

Evidently this guy is capable of writing visual, evocative prose. This paragraph is all very dramatic, and vaguely explanatory, to boot. Fair enough.

> The contradiction of an immaterial currency built on pricey matériel (custom chips and hardware, massive air conditioners, elaborate security gear) is only one of Bitcoin’s paradoxes. It’s an exquisitely logical concept and an exuberantly irrational bubble; a futuristic system that hearkens back to the gold standard; a currency where units of money have identities and humans are anonymous; a competitive model with a cooperative core. These paradoxes begin with Bitcoin’s solution to the problem of “cryptocurrencies,” forms of money that can be spent over digital networks anonymously and untraceably (and untaxably). How do we know that this digital note is worth what it claims to be worth? How do we know that the person spending it can only spend it once? (If your digital money is just a string of bits, why not copy and paste it and spend it as many times as you like?) And how do we know that the person making the digital notes won’t make so many that they become worthless?

Hey, a few questions presented in a fashion that a layman can understand. Sort of. I'm not sure it's fair to juxtapose the logical concept of Bitcoin's underlying protocol with the irrational exuberance of its speculators, though. It almost seems like an advacement of the notion that Bitcoin itself is the source of such irrationality, when it's in fact a very human element, and prevalent amongst other markets. Well, whatever. I'm probably nitpicking. Let's move on.

> Nakamoto’s answer was first announced on a mailing list for cryptographers on October 31, 2008, in the midst of the unfolding global financial crisis. “His” proposal was Bitcoin, a protocol for the collective verification of transactions. When you send a bitcoin (a string of characters with certain properties) to a payee, the transaction is broadcast out onto the peer-to-peer Bitcoin network, where it is added to a master public ledger. This ledger, the “blockchain,” becomes the historical basis for a set of mathematical problems that the computers on the network compete to solve in order to calculate and thereby “discover” newly minted bitcoins—the process called mining. The problems get incrementally harder, which prevents bitcoins from being produced too quickly; the protocol is built to become more demanding over time. There are about twelve million in circulation now, with rates of new production slowing steadily, and only twenty-one million will be produced in total. This deflationary move makes Bitcoin behave anachronistically, like, say, gold, with a fixed supply theoretically stabilizing value without state regulation. (And, like gold, Bitcoin is an extractive industry: Those megawatt hours of electricity for the computers mostly come from great ashy pits of coal.)

This is fairly informative. Maybe this is a quality article. Oh, wait. Another nitpick. Bitcoin doesn't behave like gold, nor will it -- gold is a commodity of which more enters the market each year. At the moment, more Bitcoin enters the market each year, as well. But Bitcoin will stop being generated long before we exhaust the gold supply on Earth. And, really, there's more gold in the universe, and we're making strides toward exploiting resources off-planet, leading one to conclude that additional gold will never stop entering the market. But there will never be more Bitcoin after a certain point.

> The blockchain is Bitcoin’s truly innovative component: Imagine that any given dollar, euro, or renminbi note could be unfurled like a genealogical scroll with the tale of its circulation. The humans can remain incognito, identified only by their transaction “addresses” in the network, but every transaction is seen by all and vouched for by ludicrous quantities of computational power. Yet, for all this work, what can one buy with bitcoins, without trading them for more conventional currencies? Drugs, chips at online casinos, seats on Virgin Galactic, various Web services, donations to WikiLeaks, tuition at a Cypriot university, and, in a few months, Overstock.com knickknacks—but you’d be a fool to spend them now. Bitcoin is dominated by hoarders and speculators and has been on a roller-coaster run, going from ten US dollars for a single bitcoin in early 2013 up to one thousand dollars and above on the Chinese exchanges, before losing half its value overnight and climbing back to the high seven hundreds (as of press time).

A decent explanation of the transactional record associated with each Bitcoin. Alright. And some commentary on the rampant speculation and hoarding of Bitcoin. Fair enough. It's not untrue. It's not unfounded. It's a concern that many, including me, have about the viability of Bitcoin as a proper currency. But that concern doesn't necessarily apply to all of the crypto-coins which have been developed in Bitcoin's wake. Some of them have different properties; the deflationary hoarding argument isn't necessarily valid in every case.

> Bitcoin can seem like a satire of the present moment: an extravagant waste of electricity, hardware, and venture capital for a Central Banker Fantasy Camp thrown by Soylent-drinking geek dudes who daydream of post-national libertarian seasteads with good molly and kick-ass guns. Much of the rhetoric around the currency is symptomatic of the myopia of the highly but narrowly intelligent, reflecting the assumption that being a hardworking programmer means you can solve—or “disrupt”—any given problem. International finance, interbank transfers, and smashing the state? Boom, solved, and in time for my CrossFit class. A sharply joking online neologism for bitcoins is “Dunning-Krugerrands,” after the South African gold coin and the Dunning-Kruger effect—the cognitive bias by which those less skilled or more knowledgeable in a particular field will correspondingly overestimate their abilities and understanding.

Oh.

Oh no.

At this point it's clear that this isn't intended to be an informative article. What we have here is polemic drivel. The sort of abuse of rhetoric intended to discredit something without actually providing a rational argument. The kind of thing politicians or unscrupulous journalists employ to take cheap shots. The kind of thing that's frankly irresponsible to publish.

Fine.

Let's treat it like what it is. I can get down in the mud, too.

> Bitcoin can seem like a satire of the present moment

Interesting choice of the words "can seem" when the rest of the paragraph consists of a strongly-worded and rather disparaging bit of nonsense directed at what I'll call the "programmer-type." It's almost as if the author employs these two words in an attempt to make an assertion without actually using the diction of an assertion -- i.e. "Bitcoin is X." What is this, the opinion-piece manifestation of plausible deniability? Maybe the author wants to rant without fearing that he'll be criticized for it.

> an extravagant waste of electricity, hardware, and venture capital for a Central Banker Fantasy Camp

"Extravagant waste?" "Central Banker Fantasy Camp?" Right. Your opinion is noted. Personally, I'd be interested in seeing the author make a rational, logical, and supportable argument against the value of crypto-currency as a whole. But vacuous polemic is so much easier, isn't it?

> Soylent-drinking geek dudes who daydream of post-national libertarian seasteads with good molly and kick-ass guns.

Sorry, ace. I prefer Bombay Sapphire, weed, and fast cars. And if you want to talk about seasteading, I suggest you attempt an informed debate with Peter Thiel about it, rather than pairing it with "kick-ass guns" as if only crazy redneck fringe elements might entertain the concept. Because it's entirely transparent that you're doing exactly that for the sake of, in Humanities terms, *otherizing* this "programmer-type."

> Much of the rhetoric around the currency is symptomatic of the myopia of the highly but narrowly intelligent, reflecting the assumption that being a hardworking programmer means you can solve -- or "disrupt" -- any given problem.

Um. Okay.

Disruption refers to a phenomenon that occurs in various sectors of research and industry, when one technology or approach rapidly overcomes the status-quo. It's not a problem-solving methodology. This is a real term, related to "scientific revolution." You might want to look it up.

As for programming, it entails the creation of something from, essentially, nothing. It's akin to writing in that sense. Both writing and programming can change lives and greatly impact the world. Sometimes this manifests in solving problems. That isn't an assumption; that's a historical phenomenon.

> International finance, interbank transfers, and smashing the state? Boom, solved, and in time for my CrossFit class.

Rhetoric so overblown that it comes across as delusional. Of course, that's the point. You're still otherizing the "programmer-type."

And CrossFit? Really? That yuppie shit doesn't have a place in my cyberpunk crypto-anarchist world, maaaaaaaaaan.

> the Dunning-Kruger effect -- the cognitive bias by which those less skilled or more knowledgeable in a particular field will correspondingly overestimate their abilities and understanding.

Of course. The parting shot in a paragraph which is, essentially, one giant ad-hominum attack against the kind of people supposedly in the Bitcoin and crypto communities. This is also something of a logical fallacy stemming from appeal to authority -- if there's a name for this effect, which I haven't demonstrated to actually be true in the case I'm examining, it must legitimize what I'm saying!

> If we set the currency itself aside, though, something far more interesting emerges. Nakamoto has produced a swiftly adopted system in which the whole participating community verifies the activity of its parts. There are already forks of the Bitcoin protocol to produce new blockchains, whether for alternate currencies with other social properties or for systems that can reliably authenticate acts of production and exchange on a global scale. The protocol implies new orders of cooperatively approved value, shared among strangers, managed by machines, and looking like nothing we’ve ever seen before—certainly not the all-amenities Galt’s Gulch of individualist fantasy. Paradoxical Bitcoin—this virtual, coal-fired, worthless, expensive currency, as fluid as data and as fixed as a florin—is a libertarian fetish that may yield a wealth of communal, collective ideas.

A bit more polemic in this paragraph, but nothing so eregious as the last. But if you're trying to make a point, I'm not seeing it. Unless that point is "online communities yield communal, collective ideas." Which is self-evident.

Whatever. Nothing useful here. Let's move on.

> Finn Brunton is an assistant professor in media, culture, and communication at New York University and the author of Spam: A Shadow History of the Internet (MIT Press, 2013).

Oh.

Of course. It all makes sense now.

You're a career academic with a humanities background attempting to pontificate about a technical and economic phenomenon.

> the myopia of the highly but narrowly intelligent

> the cognitive bias by which those less skilled or more knowledgeable in a particular field will correspondingly overestimate their abilities and understanding.

Take a look in the fucking mirror.

And that's *my* ad-hominum attack for the day.

But it's kind of hard to argue rationally against something that isn't a rational argument, wouldn't you say?

"This article isn't exactly top-quality," I type into my Skype chatbox.

And my day begins.
