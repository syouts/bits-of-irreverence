---
layout: post
title: "Progn in Shen"
date: 2013-07-19
categories: blog
---
If you've not yet heard of the [Shen programming language](http://shenlanguage.org), you should look into it. Shen combines ML-style pattern matching, an optional ML-style type system, and an integrated Prolog with all of the expressive power of earlier Lisps.

I've spent about an hour with it and didn't notice any imperative constructs included by default. Here's a variant of Common Lisp's "progn" implemented as a reader macro.

{% highlight scheme %}
(defmacro exec
  [exec X] -> X
  [exec F | R] -> [tail [cons F [exec | R]]])
{% endhighlight %}

{% highlight scheme %}
(exec
  (set a 1)
  (set b 2)
  (set c 3))

(value a)
=> 1
(value b)
=> 2
(value c)
=> 3
{% endhighlight %}
