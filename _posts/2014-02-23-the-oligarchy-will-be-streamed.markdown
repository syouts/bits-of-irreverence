---
layout: post
title: "The Oligarchy Will Be Streamed"
date: 2014-02-23 13:25
categories: blog
---

[ArsTechnica](http://arstechnica.com/business/2014/02/netflix-is-paying-comcast-for-direct-connection-to-network-wsj-reports/)

> Netflix has agreed to pay Comcast for a direct connection to the cable and Internet service provider's network, a move that will improve streaming video quality for Comcast customers, the Wall Street Journal reported today.

> News of a paid peering deal comes two days after a traceroute showed that the two companies were exchanging traffic with each other directly. Netflix performance on Comcast had been getting worse for months, suggesting a feud between Comcast and Netflix or between Comcast and Cogent, one of Netflix's Internet transit providers.

It makes business sense; Netflix is now large enough that it can cut a deal with the ISPs not only to prevent traffic throttling, but to gain direct access to ISP infrastructure for the purpose of improving their streaming service.

Of course there's that whole net neutrality thing. And that whole precedent thing.

But if you can just join the cartel, why try harder?
