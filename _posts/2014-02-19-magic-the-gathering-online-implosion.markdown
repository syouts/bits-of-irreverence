---
layout: post
title: "Magic the Gathering Online Implosion"
date: 2014-02-19 19:14
categories: blog
---

The ask for BTC on Mt. Gox is presently around $266. On Coinbase, it's about $629.

What's going on here?

Everyone in the community knows that Mt. Gox froze its operations as an exchange recently, and previously was sluggish with handling transactions from BTC to fiat currencies. Some speculated about solvency issues.

I'll hazard a guess about some outcomes.

### Given

Mt. Gox has suspended all withdrawals of BTC from their wallet platform.

There are three possible scenarios. I'll examine them in order.

### Surmountable Technical Issues

Wherein we accept Gox's statements regarding transaction malleability at face value.

They've locked down their exchange to work around issues.

Let's examine transaction malleability first -- this is technical. Each Bitcoin transaction sports a signature that verifies its integrity. Each Bitcoin transaction also sports a unique identifier. The identifier could be used to reference the transaction in other software, track the transaction, and so forth. Both the signature and identifier are generated using cryptographic hash functions. In layman's terms, this means that some math is employed to make sure each transaction is valid and resistant to tampering.

Transaction malleability is an issue where certain properties of the transaction can be altered such that the unique identifier -- the transaction ID -- changes *without* the signature also changing. This means that the transaction is still considered valid by the Bitcoin network, but software which relies on transaction IDs isn't viable, as transaction IDs aren't immune to tampering.

More technically, this occurs because only a subset of fields hashed to generate the TID are hashed to generate the signature. The TID-exclusive fields are the potential targets for tampering.

Transaction malleability has been a known issue with the Bitcoin protocol since 2011. So if it's the catalyst for the Mt. Gox freeze, then their software obviously relies on TIDs and their developers are to blame for it breaking under duress. Confidence in Mt. Gox will be further shaken. Perhaps irredeemably so, considering that they're no longer the only game in town.

But maybe they'll figure out their issues and return to business as usual.

### Insolvency

Wherein we assume that Gox's statements are a shield to cover for their insolvency.

This seems plausible, considering previous issues regarding withdrawals.

Why would an exchange freeze its operations to hide insolvency? Wouldn't a freeze *indicate* insolvency?

Yeah, possibly.

But Gox isn't a bank -- for every buyer of a Bitcoin, there's a seller of fiat. And vice versa. Gox isn't providing an account service, holding a reserve of currency, or conducting loans. Right?

Well, maybe.

It's possible that Gox's online wallets are just an abstraction over a bank-like pool of Bitcoin. Whenever someone conducts a withdrawal -- moves funds from a wallet on the exchange to their own private wallet -- the Bitcoin is moved out of this pool and out of the exchange.

If this pool isn't solvent, then Gox can't support withdrawals, because there's nothing there to withdraw. Thus, the freeze.

This begs the question as to why Gox doesn't have a backend that keeps individual customers' Bitcoin in seperate, um, wallets. Doesn't that seem more sensible?

Yeah, if you're not playing fast-and-loose with customers' finances or indulging in a Ponzi scheme.

But there's another possible explanation for insolvency. Maybe Gox really is suffering from attacks based off of transaction malleability. Let's assume that Gox's software relies on TIDs to track which withdrawals are associated with a customer's account. A malicious customer could put in a withdrawal request, then tamper with the resulting transaction such that the TID changes. Gox's software doesn't realize this -- it still thinks the customer hasn't withdrawn anything. So the attacker puts in another withdrawal request. And another. And bleeds the exchange dry, as Gox has to buy more Bitcoin using their cash reserves so that they can keep supporting legitimate customers.

This is a highly simplified scenario, but the point is there -- what if Gox is becoming insolvent because they've been hemmoraging funds due to attacks?

The onus is still on them -- it's their software and it's a known issue with the protocol. They've had time to develop a workaround.

Regardless, if insolvency is looming, it seems like freezing withdrawals will just delay the inevitable. If they're insolvent, they're insolvent, and they can't escape it.

Or can they?

We notice that the pricing of Bitcoins on Gox has crashed since the freeze. People are panic selling their "Goxcoins" to speculators. The speculators assume that the exchange will sort out its technical issues and the price of their newly-acquired "Goxcoins" will rise to congruency with the other exchanges. There's risk to these speculators, though -- they can't withdraw their "Goxcoins" nor can they exchange them for fiat without finding another buyer *on Mt. Gox*. The coins don't leave the Gox ecosystem. If the exchange crashes, they're gone.

This speculation isn't an arbitrage opportunity because the speculators can't actually list these coins on other exchanges, because they can't withdraw them from Gox.

But the people who run Gox have full control over their exchange and their software, right? Isn't it plausible that *they* could withdraw Bitcoin to their own wallets?

Perhaps these people have frozen withdrawals in a deliberate move to tank the price of Bitcoin on their own exchange, enabling them to purchase Bitcoin for cheap and sell them at going-rates on other exchanges. For Gox insiders, this *is* an arbitrage opportunity.

And then they can plow the money back into the business and solve their insolvency problem. Sure, their reputation will be in the toilet, but at least they're still operating.

*Caveat. If Gox's wallets are an abstraction over a pool of Bitcoin and said pool isn't solvent, then they couldn't do this, as they couldn't exchange fiat for Bitcoin that they don't actually have.*

### Exploitation

Wherein we assume that Gox's owners are defrauding their customers of currency and the freeze on exchange functionality plays a role.

There's no desire to keep the exchange afloat.

The Gox people could just take the money and run. Buy up cheaply-listed Bitcoin with personal accounts, execute arbitrage en-masse, and let the exchange implode. Who knows if they'd ever get charged for anything? It's the world of Bitcoin, and it's hardly regulated. Legislation and litigation are nascent.

In this case, the freeze would be the catalyst for their exit strategy.

### Remarks

Whatever the reality, the various scenarios are interesting to think about.

I don't have any particular fondness for Mt. Gox, and I haven't made use of their services, but I do think that the original owners of what was once a humble trading card exchange broke a lot of new ground for the Bitcoin community. It would be unfortunate for Mt. Gox to vanish and be recalled in an overwhelmingly negative light.

But the current owners aren't exactly inspiring confidence, and the world is a pretty fickle place.

Particularly when there's money involved.
