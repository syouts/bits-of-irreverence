---
layout: post
title: "GL-Pyramids"
date: 2013-07-29 11:13
categories: blog
---

A WebGL demo intended to emulate 80s-style vector graphics. There are a few issues which make me less-than-satisfied with the result.

Techniques employed: render-to-texture, radial kernel/gaussian blur, screen glow.

[Demo](http://syouts.com/demos/gl-pyramids/)

[Source Code](https://github.com/syouts/gl-pyramids)
