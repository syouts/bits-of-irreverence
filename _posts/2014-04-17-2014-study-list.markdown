---
layout: post
title: "2014 Study List"
date: 2014-04-17
categories: blog
---

I recently posted about autodidacticism. Part of being an autodidact is having self-direction and rigor in one's studies. To achieve this, one's plans require structure. Since I'm trying to become more autodidactic, I've decided to employ structure in the form of a yearly syllabus. This isn't particularly innovative, but it's one of the things that universities seem to get right.

The scope of the syllabus will be for the remainder of this year. My initial thoughts were to split my focus between studying web technologies I haven't yet had time for, and studying less directly applicable things like new programming languages and the like. But, like many developers, I drink from the technological firehose over the course of doing normal work. There's a profusion of ways to skin a CRUD app, but the core concepts remain similar. The areas where you really expand your mind are either by addressing hard problems, for which no amount of rote experience can prepare you, or by trying to enhance the way you think. These areas are what I want to study. It seems to me that the only way to avoid accepting technological Blubs is to examine technological outliers. Many of them won't be worth much, but the ones that are will *really* be worth a lot.

So how does one define an outlier? Age seems a poor metric, although there's a fixation in our industry on new, new, new. It's unclear whether technologies, like people, reproduce more numerously with each successive generation. If this is the case, then it might follow that the aggregate collection of new technologies forces older technologies to become outliers. But that doesn't seem to make a great deal of sense -- COBOL isn't used much for new projects these days, but there's still a lot of it out there. And I don't think anyone would consider COBOL to be an outlier in the "Beating the Averages" sense of the notion. Likewise, one might posit that new technologies are superior, but the existence of Node.js disproves that notion. As does the existence of JavaScript, which is what happens when Scheme is bastardized for Java programmers. If anything, the aforementioned are only outliers because of how awful they are. And yet they're widely used. Worse is better at work? \[1\]

I'm interested in looking at things that aren't necessarily widely popular. Age doesn't matter, nor does the existence of a well-established track record -- if they're not popular *right now*, they're at least worth a cursory glance. Some might have been displaced, but some -- like Lisp -- might have always existed more in the margins than on center stage. The technology adoption curve is an observation that the Next Big Thing by definition comes from the margins and explodes into popularity. Once something is popular, the mean begins to approach it; but if one can identify the Next Big Thing before it becomes popular, it can prove a tremendous advantage. Or, at least that's the operating assumption behind most startups, and the topic of Beating the Averages. \[2\]

So, like some kind of cyber-hipster, my study list isn't exactly mainstream. Since I'm also working, the pacing is fairly measured.

#Syllabus

###April

This month is fairly easy, since I already know a decent amount of Common Lisp.

I'll read Paul Graham's "ANSI Common Lisp." I'll also complete all exercises, post code on GitHub and post interesting insights on this site. Only one book this month, since most of it has already passed.

###May

["On Lisp,"](http://www.paulgraham.com/onlisp.html), also by Paul Graham, is the first text this month. It gives a more advanced treatment of macros.

Next up is ["Learn You a Haskell"](http://learnyouahaskell.com/chapters) by Miran Lipovaca.

Neither of these appear to have exercises. I'll be doing [Ninety-Nine Haskell Problems](http://www.haskell.org/haskellwiki/H-99:_Ninety-Nine_Haskell_Problems). I might need more exercises to solidify concepts, and if I do, I'll seek them out.

###June

["Real World Haskell"](http://shop.oreilly.com/product/9780596514983.do), for a practical treatment.

Taking a slant toward the theoretical, ["An Introduction to Functional Programming Through Lambda Calculus"](http://www.amazon.com/Introduction-Functional-Programming-Calculus-Mathematics/dp/0486478831).

###July

Now it gets weird.

["Thinking Forth"](http://thinking-forth.sourceforge.net/). Forth? Why? Well, because it's different, and like Lisp, has a simple, concise core. Perhaps there's some insight to be had.

["The Book of Shen, 2E"](http://www.shenlanguage.org/tbos.html) Shen is a modern Lisp integrating some ideas from ML. It will be interesting to see what I think of it once the Haskell stuff has been studied.

###August

Now we turn toward practicality.

The last book for the year will be K&R. I've done C, but I haven't read the classic.

The rest of my time will be spent developing non-trivial applications using the languages I've picked up. It's the only way to really solidify knowledge.

This month's application -- whatever it is -- will be written in Common Lisp.

###September

Haskell application.

###October

Simple Lisp interpreter. Maybe in C, maybe in a higher-level language.

Simple Forth interpreter. Same deal.

###November

Common Lisp application.

###December

Haskell application.

#Notes

\[1\] With Node, this seems to be the case. Ryan Dahl has commented that "node is popular because it allows normal people to do high concurrency servers." A simple, understandable design might not be the Right Thing, but it is often the popular thing.

\[2\] A possible protest is that Lisp hasn't claimed tremendous popularity. This is somewhat true, but there are stirrings of considerable work in this space. We've got Clojure, and Hy, both of which capitalize on large library ecosystems to ensure practicality. Common Lisp now has Quicklisp and there seems to be some effort to present a more cohesive selection of libraries to solve real world problems.
