---
layout: post
title: "Heavy Industry"
date: 2014-02-18 02:24
categories: blog
---

![Mining Rigs](/public/img/2014/02/18/heavyindustry.jpg)

My girlfriend said that these mining rigs gave her an industrial vibe.

But I'm hooking up expensive power-sucking electronics to maintain a virtual currency that exists outside of government minting. On an exposed shelf in my apartment, illuminated only by the neon glow of motherboard lights and the flickering of my monitors.

It's a Gibsonian sort of feel.
