---
layout: post
title: "Tendrils of Power"
date: 2013-07-19 15:14
categories: blog
---

[JWZ](http://www.jwz.org/blog/2013/07/opencorporates/)

> If you want to understand how complex multinational companies are, consider this. In Hong Kong, there's a company called Goldman Sachs Structured Products (Asia) Limited. It's controlled by another company called Goldman Sachs (Asia) Finance, registered in Mauritius.

> That's controlled by a company in Hong Kong, which is controlled by a company in New York, which is controlled by a company in Delaware, and that company is controlled by another company in Delaware called GS Holdings (Delaware) L.L.C. II.

> ...Which itself is a subsidiary of the only Goldman you're likely to have heard of, The Goldman Sachs Group in New York City.

> That's only one of hundreds of such chains. All told, Goldman Sachs consists of more than 4000 separate corporate entities all over the world, some of which are around ten layers of control below the New York HQ.

> Of those companies approximately a third are registered in nations that might be described as tax havens.Indeed, in the world of Goldman Sachs, the Cayman Islands are bigger than South America, and Mauritius is bigger than Africa.

[OpenCorporates](http://opencorporates.com/viz/financial/)
