---
layout: post
title: "CS Degrees Suck"
date: 2012-08-30
categories: blog
---

[Prog21](http://prog21.dadgum.com/149.html)

> Computer science should only be offered as a minor. You can major in biology, minor in computer science. Major in art, minor in computer science. But you can’t get a degree in CS.

This got me thinking. I don’t fully agree with the author, but I do believe that CS programs suck.

Why do they suck?

Their emphasis is skewed.

CS teaches a mixture of basic programming fluency, domain knowledge, and theory. It also saddles the student with plenty of peripheral requirements that are supposedly more applicable to general engineering.

Many (probably the majority) of CS grads go into software development - the one thing that CS doesn’t emphasize.

UCLA CS undergrads are required to take five quarters (a quarter being ten weeks) of mathematics, two quarters of physics, a quarter of electrical engineering, and several of our upper-level requirements are focused heavily on the theory of CS.

CS theory is basically math with a smattering of concepts from other domains, and probably best left to mathematicians with an interest in computing (we have a Mathematics of Computing major here).

The math and physics we’re taught are really only useful for a narrow subset of software development.

I’ve yet to discern any purpose for electrical engineering knowledge, when processor architecture is really the lowest-level abstraction of a computing system that’s remotely useful to a software developer.

By contrast, our program offers a single-quarter course in software engineering. It’s an elective. It’s the only course where we’re ever actually required to develop a software system of nontrivial complexity in a team environment. And even then, the size of the applications created for the course pale in comparison to real-world software systems. The rest of the time, we’re writing 1k-2k line one-off programs to solidify domain knowledge.

Cut out the peripheral requirements and remove the theory, and UCLA’s CS program could probably be condensed into a minor. The practical value of the major doesn’t scale linearly with the amount of course work, so the courses that contribute only tangential value might as well be gutted, and CS grads would be no less prepared to become software engineers. Which is to say - not very well-prepared.

So, yes. CS degrees suck. A software engineering degree wouldn’t.
