---
layout: post
title: "The Situation Room"
date: 2014-02-24 22:32
categories: blog
---

[MtGox Document](/files/2014/02/24/209050732-MtGox-Situation-Crisis-Strategy-Draft.pdf)

If this is real, it's fairly damning.

> With actual assets using arbitrage/ injecting new coins to erase them from the books. Informing and asking selected Bitcoin main players to ask for their help. The MtGox price is low, making it possible to erase a significant portion of the debt, but it needs to be done quickly. Injections in coin are most useful (enough to run the exchange) but some cash is also needed to not run a fractional reserve.

Update:

{% highlight bash %}
$ whois gox.com

...

Registrant Name: Mark Karpeles
Registrant Organization: Tibanne Co. Ltd.
Registrant Street: Round Cross Shibuya 5F
Registrant Street: 11-6 Shibuya 2-Chome
Registrant City: Shibuya
Registrant State/Province: 13
Registrant Postal Code: 150-0002
Registrant Country: Japan
Registrant Phone: +0.345501529
Registrant Phone Ext:
Registrant Fax: +0.3045206299
Registrant Fax Ext:
Registrant Email: mark@tibanne.com
Registry Admin ID:
Admin Name: Mark Karpeles
Admin Organization: Tibanne Co. Ltd.
Admin Street: Round Cross Shibuya 5F
Admin Street: 11-6 Shibuya 2-Chome
Admin City: Shibuya
Admin State/Province: 13
Admin Postal Code: 150-0002
Admin Country: Japan
Admin Phone: +0.345501529
Admin Phone Ext:
Admin Fax: +0.3045206299
Admin Fax Ext:
Admin Email: mark@tibanne.com

...

{% endhighlight %}

This should be good.

[Previously](/blog/2014/02/21/well-isnt-this-interesting/), [previously](/blog/2014/02/19/magic-the-gathering-online-implosion/).
