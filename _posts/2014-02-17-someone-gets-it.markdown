---
layout: post
title: "Someone Gets It"
date: 2014-02-17 23:34
categories: blog
---

[NYTimes](http://www.nytimes.com/2014/02/16/opinion/sunday/kristof-professors-we-need-you.html)

> A basic challenge is that Ph.D. programs have fostered a culture that glorifies arcane unintelligibility while disdaining impact and audience. This culture of exclusivity is then transmitted to the next generation through the publish-or-perish tenure process. Rebels are too often crushed or driven away.

And this is precisely why university lectures are often an absolute waste of time for anyone with auto-didactic tendencies. There's no added value or clarity when the culture from which the lecturer originates doesn't care to emphasize those ideas.

It's no coincidence that my best instructors as an undergraduate were guest lecturers from industry.
