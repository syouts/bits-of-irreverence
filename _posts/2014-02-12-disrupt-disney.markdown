---
layout: post
title: "Disrupt Disney"
date: 2014-02-12 12:20
categories: blog
---

[NYTimes](http://http://www.nytimes.com/2014/02/13/business/media/disney-plans-to-encourage-digital-start-ups.html)

A panel of Disney executives sat on the periphery of a semi-circular table. Their brows were furrowed in concentration as they listened to a proposal by a young VP.

"- so what I'm really advocating is, at the end of the day, disruptive innovation. Iteration. Agility."

An older executive chimed in, "Disruptive innovation?"

"Yeah." The VP gesticulated expansively. "Sleeping Beauty? Cinderella? They're old-school. We can't do anything with them. They don't appeal to the Facebook generation. They're the Windows XP and IBM PCs of our entertainment lineup."

A few slow, considerate nods from the executives, and the VP continued.

"We need something new. Something daring. We need to create the iPods and iPhones of animated film. Something with vision. Something with technology."

He paused to sip from a water bottle.

"By partnering with TechStars, we can tap a powerful, yet-unsolicited resource for innovative ideas."

Another pause, for dramatic effect.

"White and Asian males between the ages of eighteen and thirty. Maybe the occasional Indian."

A few more now-enthusiastic nods. The momentum was building to a climax.

"Give me the go-ahead on this, and I'll take Disney into the 21st century."

The nodding was frenzied enough to evoke some kind of farcical array of bobble heads. But one man -- a pot-bellied, tie-dye-wearing art director with a gray ponytail -- raised a hand to interject. He spoke.

"Yeah, so there's one issue with this idea."

"What's that?"

"We're creating art, not fucking Twitter clones."
