---
layout: post
title: "Tim Cook Memo Line-by-line: Really?"
date: 2012-08-25
categories: blog
---

[Jacques Mattheij, via Hacker News](http://www.jacquesmattheij.com/tim-cook-memo-line-by-line)

> And you’re doing that by virtue of the millions upon millions of man-hours that went into the real innovations, the ones by people that did not bother to patent their ideas but that simply thought that these things are too obvious to even begin to think of patenting them. Patents are being abused by companies the world over to keep the competition at bay, not to foster innovation. A patent on a grid of rounded icons? How else would you arrange them? A bit sloppy maybe, make the corners protrude? What about that pinch to zoom thing? Any suggestions on how else any half decent developer would implement that? These are not patents on innovation, they’re patents on simple ideas and features that you didn’t even think of first but you were the first to patent.

I can’t help but feel as if all of the claims made regarding “real innovation” in this post grossly undervalue the strides Apple has made in UI/UX and overall product design in favor of elevating more immediately self-evident technical advances.

The strength of Apple’s innovation has always been in delivering complete, cohesive products. In that regard, user interface details and interactions which seem trivial technically are hardly trivial in terms of user experience.

As a competitive entity, there’s no reason for Apple not to leverage every technique at its disposal to ensure its competitive viability - and one of these techniques is using patent litigation to prevent competitors from carbon-copying its innovations. Moral and ethical arguments should be made with regard to the integrity of the patent system itself, not a single corporation’s lawful use of that system.
